
local oop = require "oop"
local ffi = require "ffi"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local Object, super = oop.Class()

function Object.__init(class, x, y)
	local self = super.__init(class)
	
	self.x = x
	self.y = y
	
	return self
end

function Object:getModelMatrix()
end

function Object:draw(outline)
end

function Object:destroy()
end

return Object
