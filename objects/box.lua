
local oop = require "oop"
local ffi = require "ffi"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local Box, super = oop.Class(require "objects.object")

function Box.__init(class, x, y, w, h)
	local self = super.__init(class, x, y)
	
	self.w = w
	self.h = h
	
	return self
end

function Box:getModelMatrix()
	return Geometry.m_model:identity():translate(self.x, self.y, 0):scale(self.w, self.h, 1)
end

function Box:draw(outline)
	Geometry.drawBox(outline)
end

return Box
