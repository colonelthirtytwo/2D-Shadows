
local oop = require "oop"
local ffi = require "ffi"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local function drawCircle(cx, cy, r, segments, outline)
	if outline then
		gl.glBegin(glc.GL_LINE_LOOP)
	else
		gl.glBegin(glc.GL_POLYGON)
	end
	
	local c = 2*math.pi/segments
	for i=1,segments do
		local theta = i*c
		local dx = r*math.cos(theta)
		local dy = r*math.sin(theta)
		
		gl.glVertex2d(cx+dx, cy+dy)
	end
	
	gl.glEnd()
end

local Circle, super = oop.Class(require "objects.object")

function Circle.__init(class, x, y, r)
	local self = super.__init(class, x, y)
	self.r = r
	return self
end

function Circle:getModelMatrix()
	return Geometry.m_model:identity():translate(self.x, self.y, 0):scale(self.r, self.r, 1)
end

function Circle:draw(outline)
	Geometry.drawCircle(outline)
end

return Circle

