
local gl, glc, glu, glfw, glext = require("glfw").libraries()
local ffi = require "ffi"

local new_string = ffi.typeof("char[?]")
local src_ptr = ffi.new("const char*[1]")
local int_buffer = ffi.new("int[1]")

local function getLog(item, isshader)
	local get_status = isshader and glext.glGetShaderiv or glext.glGetProgramiv
	get_status(item, glc.GL_INFO_LOG_LENGTH, int_buffer)
	if int_buffer[0] ~= 0 then
		local log_buffer = new_string(int_buffer[0])
		local get_log = isshader and glext.glGetShaderInfoLog or glext.glGetProgramInfoLog
		get_log(item, int_buffer[0], int_buffer, log_buffer)
		return ffi.string(log_buffer, int_buffer[0])
	end
end

local Shader = {}

function Shader.new(srctbl)
	-- Allocate values
	local program = glext.glCreateProgram()
	local shaders = {}
	
	-- Compile each shader
	for typ, code in pairs(srctbl) do
		if type(typ) == "number" then
			local shader = glext.glCreateShader(typ)
			shaders[typ] = shader
			
			src_ptr[0] = code
			int_buffer[0] = #code
			glext.glShaderSource(shader, 1, src_ptr, int_buffer)
			glext.glCompileShader(shader)
			
			glext.glGetShaderiv(shader, glc.GL_COMPILE_STATUS, int_buffer)
			
			if int_buffer[0] == 0 then
				error(getLog(shader, true), 2)
			end
			
			glext.glAttachShader(program, shader)
		end
	end
	
	if srctbl.prelink then
		srctbl.prelink(program, shaders)
	end
	
	-- Link and check
	glext.glLinkProgram(program)
	
	glext.glGetProgramiv(program, glc.GL_LINK_STATUS, int_buffer)
	if int_buffer[0] == 0 then
		error(getLog(program), 2)
	end
	
	return program, shaders
end

function Shader.validate(prog)
	glext.glValidateProgram(prog)
	print(getLog(prog))
end

return Shader
