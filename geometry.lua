
local ffi = require "ffi"
local Shader = require "shader"
local Matrix = require "matrix"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local Geometry = {}

local current_vao

local CIRCLE_SEGMENTS = 20
Geometry.CIRCLE_SEGMENTS = CIRCLE_SEGMENTS

Geometry.ATTRIB_POS = 0
Geometry.ATTRIB_TEXCOORD = 1

function Geometry.init()
	
	local function addgeometry(vertexarr, verticies)
		local info = {#vertexarr, #verticies}
		for i=1,#verticies do
			vertexarr[#vertexarr+1] = verticies[i]
		end
		return info
	end
	
	local buffer_buf = ffi.new("GLuint[1]")
	glext.glGenBuffers(1,buffer_buf)
	
	local array_buf = ffi.new("GLuint[1]")
	glext.glGenVertexArrays(1, array_buf)
	
	-- -------------------------------------------------------------------------------------------------------
	
	local basic_prog = Shader.new {
		[glc.GL_VERTEX_SHADER] = [[
			#version 150
			
			uniform mat4 proj;
			uniform mat4 view;
			uniform mat4 model;
			
			in vec2 in_pos;
			out vec4 gl_Position;
			
			void main()
			{
				gl_Position = proj * (view * (model * vec4(in_pos, 0, 1)));
			}
		]],
		
		[glc.GL_FRAGMENT_SHADER] = [[
			#version 150
			
			uniform vec4 color;
			
			out vec4 gl_FragColor;
			
			void main()
			{
				gl_FragColor = color;
			}
		]],
		
		prelink = function(prog, shaders)
			glext.glBindAttribLocation(prog, Geometry.ATTRIB_POS, "in_pos")
		end
	}
	
	glext.glUseProgram(basic_prog)
	
	Geometry.shader = {
		prog = basic_prog,
		
		u_proj  = glext.glGetUniformLocation(basic_prog, "proj"),
		u_model = glext.glGetUniformLocation(basic_prog, "model"),
		u_view  = glext.glGetUniformLocation(basic_prog, "view"),
		u_color = glext.glGetUniformLocation(basic_prog, "color"),
		
		a_pos = glext.glGetAttribLocation(basic_prog, "in_pos"),
	}
	
	glext.glUniform4f(Geometry.shader.u_color, 1,1,1,1)
	
	glext.glUseProgram(0)
	-- -------------------------------------------------------------------------------------------------------
	
	Geometry.vbo = buffer_buf[0]
	Geometry.array = array_buf[0]
	
	glext.glBindBuffer(glc.GL_ARRAY_BUFFER, Geometry.vbo)
	glext.glBindVertexArray(Geometry.array)
	
	Geometry.ranges = {}
	
	local verticies = {}
	
	-- Box verticies
	Geometry.ranges.box = addgeometry(verticies, {
		{-0.5, -0.5, 0, 0},
		{ 0.5, -0.5, 1, 0},
		{ 0.5,  0.5, 1, 1},
		{-0.5,  0.5, 0, 1},
	})
	
	
	-- Circle verticies
	do
		local v = {}
		for i=0,CIRCLE_SEGMENTS-1 do
			local theta = 2*math.pi*i/CIRCLE_SEGMENTS
			local x = math.cos(theta)
			local y = math.sin(theta)
			v[i+1] = {x, y, x+0.5, y+0.5}
		end
		Geometry.ranges.circle = addgeometry(verticies, v)
	end
	
	-- Cone Verticies
	Geometry.ranges.tri = addgeometry(verticies, {
		{ 0.0, 0.0, 0, 0.5},
		{ 1.0,-0.5, 1, 0.0},
		{ 1.0, 0.5, 1, 1.0},
	})
	
	-- Upload data
	glext.glBufferData(glc.GL_ARRAY_BUFFER, ffi.sizeof("float[?][4]", #verticies), ffi.new("float[?][4]", #verticies, verticies), glc.GL_STATIC_DRAW)
	glext.glEnableVertexAttribArray(Geometry.shader.a_pos)
	glext.glVertexAttribPointer(Geometry.ATTRIB_POS, 2, glc.GL_FLOAT, false, ffi.sizeof("float[4]"), nil)
	glext.glVertexAttribPointer(Geometry.ATTRIB_TEXCOORD, 2, glc.GL_FLOAT, false, ffi.sizeof("float[4]"), ffi.cast("void*", ffi.sizeof("float[2]")))
	
	glext.glBindBuffer(glc.GL_ARRAY_BUFFER, 0)
	
	-- Pool matricies here, since they are >128 bytes and thus NYI to compile.
	Geometry.m_proj  = Matrix()
	Geometry.m_model = Matrix()
	Geometry.m_view  = Matrix()
	
	--[[
	-- Allocate a texture for atan2
	local tx_buf = ffi.new("GLuint[1]")
	gl.glGenTextures(1,tx_buf)
	Geometry.atan2_tx = tx_buf[0]
	glext.glActiveTexture(glc.GL_TEXTURE0+5)
	gl.glBindTexture(glc.GL_TEXTURE_2D, Geometry.atan2_tx)
	gl.glTexParameteri(glc.GL_TEXTURE_1D_ARRAY, glc.GL_TEXTURE_MIN_FILTER, glc.GL_LINEAR)
	gl.glTexParameteri(glc.GL_TEXTURE_1D_ARRAY, glc.GL_TEXTURE_MAG_FILTER, glc.GL_LINEAR)
	
	local tx_data = ffi.new("uint8_t[512][512]")
	for x=0,512-1 do
		for y=0,512-1 do
			tx_data[x][y] = (math.atan2((y-256)/256, (x-256)/256)/math.pi/2 + 0.5) * 255
		end
	end
	gl.glTexImage2D(glc.GL_TEXTURE_2D, 0, glc.GL_R8, 512, 512, 0, glc.GL_RED, glc.GL_UNSIGNED_BYTE, tx_data)
	glext.glActiveTexture(glc.GL_TEXTURE0)
	]]
end

function Geometry.beginDrawing(mat_proj, mat_view)
	glext.glUseProgram(Geometry.shader.prog)
	glext.glUniformMatrix4fv(Geometry.shader.u_proj, 1, false, mat_proj:getArray())
	glext.glUniformMatrix4fv(Geometry.shader.u_view, 1, false, mat_view:getArray())
end

function Geometry.endDrawing()
	glext.glUseProgram(0)
end

function Geometry.modelMatLoc()
	return Geometry.shader.u_model
end

function Geometry.setColor(r,g,b,a)
	glext.glUniform4f(Geometry.shader.u_color, r,g,b,a)
end

function Geometry.drawBox(outline)
	local range = Geometry.ranges.box
	if Geometry.instances then
		glext.glDrawArraysInstanced(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2], Geometry.instances)
	else
		gl.glDrawArrays(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2])
	end
end

function Geometry.drawCircle(outline)
	local range = Geometry.ranges.circle
	if Geometry.instances then
		glext.glDrawArraysInstanced(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2], Geometry.instances)
	else
		gl.glDrawArrays(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2])
	end
end

function Geometry.drawTri(outline)
	local range = Geometry.ranges.tri
	if Geometry.instances then
		glext.glDrawArraysInstanced(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2], Geometry.instances)
	else
		gl.glDrawArrays(outline and glc.GL_LINE_LOOP or glc.GL_TRIANGLE_FAN, range[1], range[2])
	end
end

return Geometry
