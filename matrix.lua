
local ffi = require "ffi"
local sin, cos = math.sin, math.cos

local Matrix = {}
Matrix.__index = Matrix
local new_matrix
local glarray = ffi.typeof("float*")

function Matrix:get(r,c) return self.v[r*4+c] end
function Matrix:set(r,c,v) self.v[r*4+c] = v end

function Matrix:zero()
	ffi.fill(self.v, ffi.sizeof(new_matrix))
	return self
end

--- Sets self to the identity matrix
function Matrix:identity()
	self:set(0,0, 1)
	self:set(0,1, 0)
	self:set(0,2, 0)
	self:set(0,3, 0)
	
	self:set(1,0, 0)
	self:set(1,1, 1)
	self:set(1,2, 0)
	self:set(1,3, 0)
	
	self:set(2,0, 0)
	self:set(2,1, 0)
	self:set(2,2, 1)
	self:set(2,3, 0)
	
	self:set(3,0, 0)
	self:set(3,1, 0)
	self:set(3,2, 0)
	self:set(3,3, 1)
	
	return self
end

function Matrix:fromtable(tbl)
	self:set(0,0, tbl[1])
	self:set(0,1, tbl[2])
	self:set(0,2, tbl[3])
	self:set(0,3, tbl[4])
	self:set(1,0, tbl[5])
	self:set(1,1, tbl[6])
	self:set(1,2, tbl[7])
	self:set(1,3, tbl[8])
	self:set(2,0, tbl[9])
	self:set(2,1, tbl[10])
	self:set(2,2, tbl[11])
	self:set(2,3, tbl[12])
	self:set(3,0, tbl[13])
	self:set(3,1, tbl[14])
	self:set(3,2, tbl[15])
	self:set(3,3, tbl[16])
	return self
end

--- Sets self to an orthographic projection matrix
function Matrix:ortho(left, right, bottom, top, near, far)
	self:set(0,0, 2/(right-left))
	self:set(0,1, 0)
	self:set(0,2, 0)
	self:set(0,3, 0)
	
	self:set(1,0, 0)
	self:set(1,1, 2/(top-bottom))
	self:set(1,2, 0)
	self:set(1,3, 0)
	
	self:set(2,0, 0)
	self:set(2,1, 0)
	self:set(2,2, -2/(far-near))
	self:set(2,3, 0)
	
	self:set(3,0, -(left+right)/(right-left))
	self:set(3,1, -(top+bottom)/(top-bottom))
	self:set(3,2, -(far+near)/(far-near))
	self:set(3,3, 1)
	
	return self
end

--- Sets self to a perspective projection matrix
function Matrix:perspective(fovy, aspect, near, far)
	local tanfov = math.tan(fovy / 2)
	
	self:set(0,0, 1/(aspect * tanfov))
	self:set(0,1, 0)
	self:set(0,2, 0)
	self:set(0,3, 0)
	
	self:set(1,0, 0)
	self:set(1,1, 1/(tanfov))
	self:set(1,2, 0)
	self:set(1,3, 0)
	
	self:set(2,0, 0)
	self:set(2,1, 0)
	self:set(2,2, -(far+near)/(far-near))
	self:set(2,3, -1)
	
	self:set(3,0, 0)
	self:set(3,1, 0)
	self:set(3,2, -(2*far*near)/(far-near))
	self:set(3,3, 0)
	return self
end

--- Sets self to a lookAt matrix (like gluLookAt, but takes directions instead of center)
function Matrix:lookAt(eyex, eyey, eyez, dirx, diry, dirz, upx, upy, upz)
	local z0, z1, z2 = -dirx, -diry, -dirz
	local x0, x1, x2 = upy*z2 - upz*z1, upz*z0 - upx*z2, upx*z1 - upy*z0
	local y0, y1, y2 = z1*x2-z2*x1, z2*x0-z0*x2, z0*x1-z1*x0
	self:set(0,0, x0)
	self:set(0,1, y0)
	self:set(0,2, z0)
	self:set(0,3, 0)

	self:set(1,0, x1)
	self:set(1,1, y1)
	self:set(1,2, z1)
	self:set(1,3, 0)

	self:set(2,0, x2)
	self:set(2,1, y2)
	self:set(2,2, z2)
	self:set(2,3, 0)

	self:set(3,0, -(x0*eyex+x1*eyey+x2*eyez))
	self:set(3,1, -(y0*eyex+y1*eyey+y2*eyez))
	self:set(3,2, -(z0*eyex+z1*eyey+z2*eyez))
	self:set(3,3, 1)

	return self
end

--- Multiplies one matrix by another
function Matrix:__mul(other)
	local dest = new_matrix()
	return dest:mulInto(self, other)
end

function Matrix:mulInto(a, b)
	self:set(0,0, b:get(0,0) * a:get(0,0) + b:get(0,1) * a:get(1,0) + b:get(0,2) * a:get(2,0) + b:get(0,3) * a:get(3,0))
	self:set(0,1, b:get(0,0) * a:get(0,1) + b:get(0,1) * a:get(1,1) + b:get(0,2) * a:get(2,1) + b:get(0,3) * a:get(3,1))
	self:set(0,2, b:get(0,0) * a:get(0,2) + b:get(0,1) * a:get(1,2) + b:get(0,2) * a:get(2,2) + b:get(0,3) * a:get(3,2))
	self:set(0,3, b:get(0,0) * a:get(0,3) + b:get(0,1) * a:get(1,3) + b:get(0,2) * a:get(2,3) + b:get(0,3) * a:get(3,3))
	self:set(1,0, b:get(1,0) * a:get(0,0) + b:get(1,1) * a:get(1,0) + b:get(1,2) * a:get(2,0) + b:get(1,3) * a:get(3,0))
	self:set(1,1, b:get(1,0) * a:get(0,1) + b:get(1,1) * a:get(1,1) + b:get(1,2) * a:get(2,1) + b:get(1,3) * a:get(3,1))
	self:set(1,2, b:get(1,0) * a:get(0,2) + b:get(1,1) * a:get(1,2) + b:get(1,2) * a:get(2,2) + b:get(1,3) * a:get(3,2))
	self:set(1,3, b:get(1,0) * a:get(0,3) + b:get(1,1) * a:get(1,3) + b:get(1,2) * a:get(2,3) + b:get(1,3) * a:get(3,3))
	self:set(2,0, b:get(2,0) * a:get(0,0) + b:get(2,1) * a:get(1,0) + b:get(2,2) * a:get(2,0) + b:get(2,3) * a:get(3,0))
	self:set(2,1, b:get(2,0) * a:get(0,1) + b:get(2,1) * a:get(1,1) + b:get(2,2) * a:get(2,1) + b:get(2,3) * a:get(3,1))
	self:set(2,2, b:get(2,0) * a:get(0,2) + b:get(2,1) * a:get(1,2) + b:get(2,2) * a:get(2,2) + b:get(2,3) * a:get(3,2))
	self:set(2,3, b:get(2,0) * a:get(0,3) + b:get(2,1) * a:get(1,3) + b:get(2,2) * a:get(2,3) + b:get(2,3) * a:get(3,3))
	self:set(3,0, b:get(3,0) * a:get(0,0) + b:get(3,1) * a:get(1,0) + b:get(3,2) * a:get(2,0) + b:get(3,3) * a:get(3,0))
	self:set(3,1, b:get(3,0) * a:get(0,1) + b:get(3,1) * a:get(1,1) + b:get(3,2) * a:get(2,1) + b:get(3,3) * a:get(3,1))
	self:set(3,2, b:get(3,0) * a:get(0,2) + b:get(3,1) * a:get(1,2) + b:get(3,2) * a:get(2,2) + b:get(3,3) * a:get(3,2))
	self:set(3,3, b:get(3,0) * a:get(0,3) + b:get(3,1) * a:get(1,3) + b:get(3,2) * a:get(2,3) + b:get(3,3) * a:get(3,3))
	return self
end

--- Multiplies the vector (x,y,z,1) by the matrix
function Matrix:mulVec(x, y, z)
	return x*self:get(0,0) + y*self:get(1,0) + z*self:get(2,0) + self:get(3,0),
		x*self:get(0,1) + y*self:get(1,1) + z*self:get(2,1) + self:get(3,1),
		x*self:get(0,2) + y*self:get(1,2) + z*self:get(2,2) + self:get(3,2),
		x*self:get(0,3) + y*self:get(1,3) + z*self:get(2,3) + self:get(3,3)
end

--- Adds a translation transform to self
function Matrix:translate(x,y,z)
	self:set(3,0, self:get(0,0)*x + self:get(1,0)*y + self:get(2,0)*z + self:get(3,0))
	self:set(3,1, self:get(0,1)*x + self:get(1,1)*y + self:get(2,1)*z + self:get(3,1))
	self:set(3,2, self:get(0,2)*x + self:get(1,2)*y + self:get(2,2)*z + self:get(3,2))
	self:set(3,3, self:get(0,3)*x + self:get(1,3)*y + self:get(2,3)*z + self:get(3,3))
	return self
end

--- Adds a scale transform to self
function Matrix:scale(x,y,z)
	self:set(0,0, self:get(0,0)*x)
	self:set(0,1, self:get(0,1)*x)
	self:set(0,2, self:get(0,2)*x)
	self:set(0,3, self:get(0,3)*x)
	
	self:set(1,0, self:get(1,0)*y)
	self:set(1,1, self:get(1,1)*y)
	self:set(1,2, self:get(1,2)*y)
	self:set(1,3, self:get(1,3)*y)
	
	self:set(2,0, self:get(2,0)*z)
	self:set(2,1, self:get(2,1)*z)
	self:set(2,2, self:get(2,2)*z)
	self:set(2,3, self:get(2,3)*z)
	
	return self
end

--- Adds a rotate transform to self
function Matrix:rotate(theta, x,y,z)
	local selfc = self:copy()
	local rotmatrix = new_matrix()
	local s, c, t = sin(theta), cos(theta), 1-cos(theta)
	rotmatrix:set(0,0, x*x*t+c)
	rotmatrix:set(0,1, y*x*t+z*s)
	rotmatrix:set(0,2, z*x*t-y*s)
	rotmatrix:set(1,0, x*y*t-z*s)
	rotmatrix:set(1,1, y*y*t+c)
	rotmatrix:set(1,2, z*y*t+x*s)
	rotmatrix:set(2,0, x*z*t+y*s)
	rotmatrix:set(2,1, y*z*t-x*s)
	rotmatrix:set(2,2, z*z*t+c)
	rotmatrix:set(3,3, 1)

	for r=0,2 do -- Don't need to multiply last row
		for c=0,3 do
			self:set(r,c, selfc:get(r,0) * rotmatrix:get(0,c) +
				selfc:get(r,1) * rotmatrix:get(1,c) +
				selfc:get(r,2) * rotmatrix:get(2,c) +
				selfc:get(r,3) * rotmatrix:get(3,c))
		end
	end
	return self
end

--- Creates a copy of self
function Matrix:copy()
	return new_matrix(self)
end

function Matrix:getArray()
	return ffi.cast(glarray, self.v)
end

function Matrix:__tostring()
	local s = ""
	for r=0,3 do
		s = string.format("%s%s% 9.4f,% 9.4f,% 9.4f,% 9.4f}%s\n", s,
			r == 0 and "{{" or " {",
			self:get(r,0), self:get(r,1), self:get(r,2), self:get(r,3),
			r ~= 3 and "," or "}")
	end
	return s
end

new_matrix = ffi.metatype("struct { float v[16]; }", Matrix)

--[[do
	-- Testing
	local m = new_matrix()
	m:identity()
	print("identity:")
	print(m, "\n")

	m:translate(1,1,1)
	print("translated 1,1,1:")
	print(m, "\n")

	m:scale(2,2,2)
	print("scale 2,2,2:")
	print(m, "\n")

	m:translate(4,4,4)
	print("translate 4,4,4")
	print(m, "\n")

	m:identity()
	m:ortho(0, 1, 0, 1, 0, 1)
	print("ortho: 0:1, 0:1, 0:1:")
	print(m)

	m:identity()
	m:translate(1,0,0)
	m:rotate(math.rad(90), 0,1,0)
	print("identity, translate (1,0,0), rotate 90deg,(0,1,0):")
	print(m)
end]]

return new_matrix
