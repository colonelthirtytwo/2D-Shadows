--local ok, err = xpcall(function()

local SCENES = {
	"largebox",
	"singlebox",
	"spotlighttest",
	"dirlighttest",
	"boxring",
	"circleorbit",
	"rings",
	"stressring",
	"stress",
	
	current = nil,
	current_index = nil,
}

math.randomseed(os.time())

local ffi = require "ffi"
local bit = require "bit"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local Renderer = require "depthmap.main"

local SCREEN_W, SCREEN_H = 1024, 768

lj_glfw.init()

lj_glfw.hint(glc.GLFW_CONTEXT_VERSION_MAJOR, 3)
lj_glfw.hint(glc.GLFW_CONTEXT_VERSION_MINOR, 1)
--lj_glfw.hint(glc.GLFW_OPENGL_PROFILE, glc.GLFW_OPENGL_CORE_PROFILE)
lj_glfw.hint(glc.GLFW_RESIZABLE, false)
--lj_glfw.hint(glc.GLFW_OPENGL_DEBUG_CONTEXT, true)

local window = lj_glfw.Window(SCREEN_W, SCREEN_H, "Lighting demo")
SCREEN_W, SCREEN_H = window:getFramebufferSize()
window:makeContextCurrent()

require("gldebug")

require("geometry").init()

--lj_glfw.swapInterval(1)
lj_glfw.swapInterval(0)

Renderer.init(window)

local last_time = lj_glfw.getTime()
local num_frames = 0

local function loadScene(i)
	i = ((i-1) % #SCENES)+1
	
	if SCENES.current then
		SCENES.current:destroy()
	end
	
	SCENES.current_index = i
	SCENES.current = require("scenes."..SCENES[i])(Renderer)
	lj_glfw.setTime(0)
	
	last_time = 0
	num_frames = 0
	
	print("Loaded scene", SCENES[i])
end
loadScene(1)

window:setKeyCallback(function(window, key, scancode, action, mods)
	if action == glc.GLFW_PRESS then
		if key == glc.GLFW_KEY_ESCAPE then
			window:setShouldClose(true)
		elseif key == glc.GLFW_KEY_UP then
			loadScene(SCENES.current_index+1)
		elseif key == glc.GLFW_KEY_DOWN then
			loadScene(SCENES.current_index-1)
		end
	end
end)

while not window:shouldClose() do
	gl.glClear(bit.bor(glc.GL_COLOR_BUFFER_BIT, glc.GL_DEPTH_BUFFER_BIT))
	
	SCENES.current:think(window, lj_glfw.getTime())
	Renderer.draw(window, SCENES.current)
	
	window:swapBuffers()
	lj_glfw.pollEvents()
	
	num_frames = num_frames + 1
	local t = lj_glfw.getTime()
	if t - last_time >= 1.0 then
		print("s/frame:", 1/num_frames, "FPS:", num_frames)
		last_time = t
		num_frames = 0
	end
end
window:destroy()
lj_glfw.terminate()

--end, debug.traceback) if not ok then print(err) io.read(1) os.exit(1) end
