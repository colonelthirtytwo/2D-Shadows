
CC=gcc
CFLAGS=-O2 -Wall
LUAJIT_INCLUDE=/usr/local/include/luajit-2.1/
LUAJIT_LIB=luajit-5.1

LUABC_SRC=$(wildcard *.lua) $(wildcard */*.lua)
LUABC=$(LUABC_SRC:.lua=.bc.c)

main.exe: main_exec.c $(LUABC)
	#$(CC) $(CFLAGS) main_exec.c $(LUABC) -lluajit-5.1 -o main.exe
	$(CC) $(CFLAGS) main_exec.c $(LUABC) -l$(LUAJIT_LIB) -I $(LUAJIT_INCLUDE) -o main.exe

%.bc.c: %.lua
	luajit -b -n "$(subst /,.,$(subst .lua,,$<))" $< $@

asdf:
	echo $(ASDF)

clean:
	rm -f $(LUABC) main.exe

.PHONY: clean
