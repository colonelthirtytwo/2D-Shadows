
local oop = require "oop"
local LightBase = require "depthmap.lightbase"
local PointLight = require "depthmap.pointlight"
local MatrixLight = require "depthmap.matrixlight"
local DirLight = require "depthmap.dirlight"
local SpotLight = require "depthmap.spotlight"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()
local Matrix = require "matrix"
local Geometry = require "geometry"

local Main = {}

local m_proj = Matrix()
local m_view = Matrix()

function Main.init(window)
	LightBase.Init(window:getFramebufferSize())
	PointLight.Init()
	MatrixLight.Init()
	gl.glClearColor(0,0,0,0)
end

function Main.createLight(typ, ...)
	if typ == "point" then
		return PointLight(...)
	elseif typ == "directional" then
		return DirLight(...)
	elseif typ == "spot" then
		return SpotLight(...)
	else
		error("Unknown light type: "..typ, 2)
	end
end

function Main.draw(window, scene)
	local screen_w, screen_h = window:getFramebufferSize()
	
	m_proj:ortho(-screen_w/2, screen_w/2, -screen_h/2, screen_h/2, -10, 10)
	m_view:identity()
	
	local lights_point, lights_matrix = {}, {}
	
	-- Sort lights by category
	for i,l in ipairs(scene.lights) do
		local cls = oop.ClassOf(l)
		if cls == PointLight then
			table.insert(lights_point, l)
		elseif cls == DirLight or cls == SpotLight then
			table.insert(lights_matrix, l)
		else
			assert(false)
		end
	end
	
	-- Upload point light data (position, radius, layer).
	local light_data = PointLight.BeginUploadData()
	for i,l in ipairs(lights_point) do
		l:uploadData(light_data, i)
	end
	PointLight.EndUploadData(light_data)
	
	-- Upload matrix light data (matrix, layer).
	light_data = MatrixLight.BeginUploadData()
	for i,l in ipairs(lights_matrix) do
		l:uploadData(light_data, i)
	end
	MatrixLight.EndUploadData()
	
	-- Preprocess lights
	LightBase.BeginRender()
	PointLight.Preprocess(scene.objects, #lights_point)
	MatrixLight.Preprocess(scene.objects, #lights_matrix)
	LightBase.EndRender(screen_w, screen_h)
	
	-- Draw to gbuffer
	gl.glClearColor(1,1,1,1)
	LightBase.BeginGBuffer(m_proj, m_view)
	Geometry.setColor(0,0,1,1)
	local model_m_loc = Geometry.modelMatLoc()
	for _,o in ipairs(scene.objects) do
		local m = o:getModelMatrix() -- Do not inline with next line; matrix needs to stay alive to prevent gc
		glext.glUniformMatrix4fv(model_m_loc, 1, false, m:getArray())
		o:draw()
	end
	
	Geometry.setColor(1,1,0,1)
	local model_m = Geometry.m_model
	for _,l in ipairs(scene.lights) do
		model_m:identity():translate(l.x, l.y, 0):scale(20,20,1)
		glext.glUniformMatrix4fv(model_m_loc, 1, false, model_m:getArray())
		Geometry.drawBox()
	end
	LightBase.EndGBuffer()
	gl.glClearColor(0,0,0,0)
	
	-- Draw lit scene
	glext.glActiveTexture(glc.GL_TEXTURE0)
	gl.glBindTexture(glc.GL_TEXTURE_1D_ARRAY, LightBase.ShadowTex)
	glext.glActiveTexture(glc.GL_TEXTURE0+1)
	gl.glBindTexture(glc.GL_TEXTURE_2D, LightBase.GBufferColorTex)
	
	gl.glEnable(glc.GL_BLEND)
	gl.glBlendFunc(glc.GL_SRC_ALPHA, glc.GL_ONE_MINUS_SRC_ALPHA)
	glext.glBlendEquation(glc.GL_MAX)
	
	PointLight.BeginDraw(m_proj, m_view, screen_w, screen_h)
	for _,l in ipairs(lights_point) do
		l:drawLightmap()
	end
	PointLight.EndDraw()
	
	MatrixLight.BeginDraw(m_proj, m_view, screen_w, screen_h)
	for _,l in ipairs(lights_matrix) do
		l:drawLightmap()
	end
	MatrixLight.EndDraw()
	
	gl.glBindTexture(glc.GL_TEXTURE_2D, 0)
	glext.glActiveTexture(glc.GL_TEXTURE0)
	gl.glDisable(glc.GL_BLEND)
	gl.glBindTexture(glc.GL_TEXTURE_1D_ARRAY, 0)
	
	-- Draw objects
	--[[
	Geometry.beginDrawing(m_proj, m_view)
	Geometry.setColor(1,1,1,1)
	local model_m_loc = Geometry.modelMatLoc()
	for _,o in ipairs(scene.objects) do
		local m = o:getModelMatrix() -- Do not inline with next line; matrix needs to stay alive to prevent gc
		glext.glUniformMatrix4fv(model_m_loc, 1, false, m:getArray())
		o:draw()
	end
	
	-- Draw light icons
	Geometry.setColor(1,1,0,1)
	local model_m = Geometry.m_model
	for _,l in ipairs(scene.lights) do
		model_m:identity():translate(l.x, l.y, 0):scale(20,20,1)
		glext.glUniformMatrix4fv(model_m_loc, 1, false, model_m:getArray())
		Geometry.drawBox()
	end
	
	Geometry.endDrawing()
	]]
end

return Main
