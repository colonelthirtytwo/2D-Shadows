
local oop = require "oop"
local ffi = require "ffi"
local bit = require "bit"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()
local Shader = require "shader"
local LightBase = require "depthmap.lightbase"

local Light, super = oop.Class(LightBase)

local UBO_BINDING_POINT = 1

local depth_shader, light_shader, shadow_ubo
local lightinfo_t = ffi.typeof([[struct __attribute__ ((packed, aligned(16))) {
	float pos[2] __attribute__ ((aligned(8)));
	float radius __attribute__ ((aligned(4)));
	int layer __attribute__ ((aligned(4)));
}]])
local lightinfo_pt = ffi.typeof("$*", lightinfo_t)

local m_proj = Matrix():ortho(-math.pi, math.pi, -0.1, 0.1, 0, 1)
local m_view = Matrix():identity()

function Light.Init()
	assert(not depth_shader)
	
	-- -------------------------------------------------------------------------------------------------------
	local ubo_buf = ffi.new("GLuint[1]")
	glext.glGenBuffers(1, ubo_buf)
	shadow_ubo = ubo_buf[0]
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, shadow_ubo)
	
	-- -------------------------------------------------------------------------------------------------------
	
	local depth_shader_prog = Shader.new {
		[glc.GL_VERTEX_SHADER] = [[

			#version 150
			#define PI 3.1415926535897932384626433832795
			#define MAX_LIGHTS ]]..LightBase.MAX_LIGHTS..[[
			
			uniform mat4 view;
			uniform mat4 model;
			
			struct LightInfo {
				vec2 pos;
				float radius;
				int layer;
			};
			
			layout (std140) uniform LightBlock
			{
				LightInfo lights[MAX_LIGHTS];
			};
			
			in vec2 in_pos;
			in int gl_InstanceID;
			
			out vec4 gl_Position;
			
			// The evil 2*pi wraparound makes lines like (-1,-1)-(1,-1) transform into (a*pi,b)-(-a*pi,b),
			// which spans the visible portion of the depth buffer even though the line is behind the section.
			// This is a hack-y interpolation trick for determinine which hemisphere the line endpoints reside on.
			// 1 = 'front' (visible), 0 = 'back' (clipped)
			// Wrapping only occurs if both verticies are in the back hemisphere, so we can discard the line
			// (or in this case their fragments) if that is the case. If the verticies didn't actually wrap,
			// they aren't visible anyway.
			// (Note that there's an edge case for lines perpendicular to the hemisphere, ex. (0,-1)-(0,1).
			// Though in that case, how the lighting spills out is arbitrary anyway).
			
			out int texlayer;
			out vec2 rel_pos;
			out float radius;
			
			vec2 polar_projection(vec2 v)
			{
				return vec2(
					atan(v.x, v.y),
					length(v)
				);
			}

			void main()
			{
				vec4 mv_pos = view * (model * vec4(in_pos, 0, 1));
				vec4 lv_pos = view * vec4(lights[gl_InstanceID].pos, 0, 1);
				rel_pos = vec2(mv_pos) - vec2(lv_pos);
				vec2 polar = polar_projection(rel_pos);
				
				radius = lights[gl_InstanceID].radius;
				gl_Position = vec4(polar.x * (1/PI), 0, polar.y/radius*2-1, 1);
				texlayer = lights[gl_InstanceID].layer;
			}
		]],
		[glc.GL_GEOMETRY_SHADER] = [[
			#version 150
			
			#define PI 3.1415926535897932384626433832795
			
			layout(lines) in;
			layout(line_strip, max_vertices=4) out;
			
			in vec2 rel_pos[];
			in int texlayer[];
			in float radius[];
			
			flat out float o_len;
			flat out float o_phi;
			smooth out float o_theta;
			
			float crossprod(vec2 u, vec2 v) {
				return u.x*v.y - u.y*v.x;
			}
			
			float angle(const vec2 u, const vec2 v) {
				return acos( dot(u, v) / (length(v)*length(u)) );
			}
			
			void main()
			{
				float s_len = gl_in[0].gl_Position.z*0.5+0.5;
				float s_phi = angle(rel_pos[1]-rel_pos[0], -rel_pos[0]);
				float s_theta = abs(gl_in[1].gl_Position.x - gl_in[0].gl_Position.x) * PI;
				
				// Always emit first vertex
				gl_Layer = texlayer[0];
				gl_Position = gl_in[0].gl_Position;
				o_phi = s_phi;
				o_len = s_len;
				o_theta = 0;
				EmitVertex();
				
				// Check for wraparound by doing a segment+ray intersection with the ray <0,0> <0,-1>
				// local den = rp0.x - rp1.x -- cross(rp1-rp0, vec(0,-1))
				// local t = rp0.x / den --cross(-rp0, vec(0,-1)) / den
				// local u = cross(-rp0, rp1-rp0) / den
				
				float den = rel_pos[0].x - rel_pos[1].x;
				float t   = rel_pos[0].x / den;
				float u   = crossprod(-rel_pos[0], rel_pos[1] - rel_pos[0]) / den;
				
				if(den != 0 && u > 0 && t > 0 && t < 1) {
					// Split vertex across boundary
					
					o_len = s_len;
					
					vec2 intersection = mix(rel_pos[0], rel_pos[1], t);
					s_len = length(intersection) / radius[0];
					
					gl_Layer = texlayer[0];
					gl_Position = vec4(-sign(rel_pos[1].x), 0, s_len * 2 - 1, 1);
					o_phi = s_phi;
					o_theta = abs(-sign(rel_pos[1].x) - gl_in[0].gl_Position.x) * PI;
					EmitVertex();
					
					EndPrimitive();
					
					s_phi = angle(rel_pos[1]-intersection, -intersection);
					
					gl_Layer = texlayer[1];
					gl_Position = vec4(sign(rel_pos[1].x), 0, s_len * 2 - 1, 1);
					o_phi = s_phi;
					o_len = s_len;
					o_theta = 0;
					EmitVertex();
					
					s_theta = abs(sign(rel_pos[1].x) - gl_in[1].gl_Position.x) * PI;
				}
				
				// Always emit last vertex and primitive
				gl_Layer = texlayer[1];
				gl_Position = gl_in[1].gl_Position;
				o_phi = s_phi;
				o_len = s_len;
				o_theta = s_theta;
				EmitVertex();
				
				EndPrimitive();
			}
		]],
		[glc.GL_FRAGMENT_SHADER] = [[
			#version 150
			
			smooth in float o_theta;
			flat in float o_len;
			flat in float o_phi;
			out vec4 gl_FragColor;
			
			void main()
			{
				gl_FragDepth = (o_len * sin(o_phi) / sin(o_phi + o_theta));
				gl_FragColor = vec4(1,1,1,1);
			}
		]],
	}
	
	glext.glUseProgram(depth_shader_prog)
	
	depth_shader = {
		prog = depth_shader_prog,
		
		u_proj  = glext.glGetUniformLocation(depth_shader_prog, "proj"),
		u_model = glext.glGetUniformLocation(depth_shader_prog, "model"),
		u_view  = glext.glGetUniformLocation(depth_shader_prog, "view"),
		
		u_flip     = glext.glGetUniformLocation(depth_shader_prog, "flip"),
		
		a_pos = glext.glGetAttribLocation(depth_shader_prog, "in_pos"),
		
		b_light = glext.glGetUniformBlockIndex(depth_shader_prog, "LightBlock"),
	}
	
	glext.glUniformBlockBinding(depth_shader_prog, depth_shader.b_light, UBO_BINDING_POINT)
	
	glext.glBufferData(glc.GL_UNIFORM_BUFFER, ffi.sizeof(lightinfo_t)*LightBase.MAX_LIGHTS, nil, glc.GL_STREAM_DRAW)
	glext.glBindBufferBase(glc.GL_UNIFORM_BUFFER, UBO_BINDING_POINT, shadow_ubo)
	
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, 0)
	-- -------------------------------------------------------------------------------------------------------
	
	local light_shader_prog = Shader.new {
		[glc.GL_VERTEX_SHADER] = [[

			#version 150
			
			uniform mat4 proj;
			uniform mat4 view;
			uniform mat4 model;
			
			uniform vec2 light_pos;
			
			in vec2 in_pos;
			
			out vec4 gl_Position;
			out vec2 pos;
			
			void main()
			{
				vec4 fpos = view * (model * vec4(in_pos, 0, 1));
				gl_Position = proj * fpos;
				pos = vec2(fpos) - vec2(view*vec4(light_pos,0,1));
			}
		]],
		[glc.GL_FRAGMENT_SHADER] = [[

			#version 150
			#define PI 3.1415926535897932384626433832795
			#define LIGHT_BLEED_FACTOR 25
			
			uniform sampler1DArray depthmap;
			uniform sampler2D g_diffuse;
			//uniform sampler2D tx_atan2;
			
			uniform float radius;
			uniform vec4 color;
			uniform int texlayer;
			uniform vec2 screensize;
			
			smooth in vec2 pos;
			in vec4 gl_FragCoord;
			out vec4 gl_FragColor;
			
			void main()
			{
				//float theta = texture(tx_atan2, pos/(len*2)+vec2(0.5,0.5)).r;
				
				float len = length(pos);
				float percent = len / radius;
				float theta = atan(pos.x, pos.y) * (0.5/PI) - (1.0/2.0);
				// = (atan(pos.x, pos.y)-PI/2)/(2*PI)
				
				float amnt = clamp( 1 - (percent - texture(depthmap, vec2(theta, texlayer)).x)*LIGHT_BLEED_FACTOR, 0, 1);
				amnt = amnt*(1-percent);
				
				gl_FragColor = texture(g_diffuse, vec2(gl_FragCoord)/screensize) * color * amnt;
			}
		]],
	}
	
	glext.glUseProgram(light_shader_prog)
	glext.glUniform1i(glext.glGetUniformLocation(light_shader_prog, "depthmap"), 0)
	glext.glUniform1i(glext.glGetUniformLocation(light_shader_prog, "g_diffuse"), 1)
	glext.glUniform1i(glext.glGetUniformLocation(light_shader_prog, "tx_atan2"), 5)
	glext.glUniform4f(glext.glGetUniformLocation(light_shader_prog, "color"), 1,1,1,0.2)
	
	light_shader = {
		prog = light_shader_prog,
		
		u_proj  = glext.glGetUniformLocation(light_shader_prog, "proj"),
		u_model = glext.glGetUniformLocation(light_shader_prog, "model"),
		u_view  = glext.glGetUniformLocation(light_shader_prog, "view"),
		
		u_pos        = glext.glGetUniformLocation(light_shader_prog, "light_pos"),
		u_r          = glext.glGetUniformLocation(light_shader_prog, "radius"),
		u_color      = glext.glGetUniformLocation(light_shader_prog, "color"),
		u_texlayer   = glext.glGetUniformLocation(light_shader_prog, "texlayer"),
		u_screensize = glext.glGetUniformLocation(light_shader_prog, "screensize"),
		
		a_pos = glext.glGetAttribLocation(light_shader_prog, "in_pos"),
	}
	
	-- -------------------------------------------------------------------------------------------------------
	glext.glUseProgram(0)
end

function Light.__init(class,x,y,r)
	local self = super.__init(class)
	self.x = x
	self.y = y
	self.r = r
	return self
end

function Light.BeginUploadData()
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, shadow_ubo)
	local ptr = glext.glMapBufferRange(glc.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightinfo_t)*LightBase.MAX_LIGHTS, bit.bor(glc.GL_MAP_WRITE_BIT, glc.GL_MAP_INVALIDATE_BUFFER_BIT))
	assert(ptr ~= nil)
	return ffi.cast(lightinfo_pt, ptr)
end

function Light.EndUploadData()
	assert(glext.glUnmapBuffer(glc.GL_UNIFORM_BUFFER) ~= 0)
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, 0)
end

function Light:uploadData(data, i)
	data[i-1].pos[0] = self.x
	data[i-1].pos[1] = self.y
	data[i-1].radius = self.r
	data[i-1].layer  = self.layer
end

function Light.Preprocess(objects, num_lights)
	-- Set up
	glext.glUseProgram(depth_shader.prog)
	glext.glUniformMatrix4fv(depth_shader.u_proj, 1, false, m_proj:getArray())
	glext.glUniformMatrix4fv(depth_shader.u_view, 1, false, m_view:getArray())
	Geometry.instances = num_lights
	
	-- Render shadow map
	gl.glViewport(0,0,LightBase.SHADOW_RESOLUTION,1)
	for _,obj in ipairs(objects) do
		local m = obj:getModelMatrix()
		glext.glUniformMatrix4fv(depth_shader.u_model, 1, false, m:getArray())
		obj:draw(true)
	end
	
	--[[
	-- Render shadow map 1
	gl.glViewport(0,0,LightBase.SHADOW_RESOLUTION/2,1)
	glext.glUniform1f(depth_shader.u_flip, -1)
	for _,obj in ipairs(objects) do
		local m = obj:getModelMatrix()
		glext.glUniformMatrix4fv(depth_shader.u_model, 1, false, m:getArray())
		obj:draw(true)
	end
	
	-- Render shadow map 2
	gl.glViewport(LightBase.SHADOW_RESOLUTION/2,0,LightBase.SHADOW_RESOLUTION/2,1)
	glext.glUniform1f(depth_shader.u_flip,  1)
	for _,obj in ipairs(objects) do
		local m = obj:getModelMatrix()
		glext.glUniformMatrix4fv(depth_shader.u_model, 1, false, m:getArray())
		obj:draw(true)
	end
	]]
	
	-- Clean up
	glext.glUseProgram(0)
	Geometry.instances = nil
end

function Light.BeginDraw(m_proj, m_view, screenw, screenh)
	glext.glUseProgram(light_shader.prog)
	
	glext.glUniformMatrix4fv(light_shader.u_proj, 1, false, m_proj:getArray())
	glext.glUniformMatrix4fv(light_shader.u_view, 1, false, m_view:getArray())
	glext.glUniform2f(light_shader.u_screensize, screenw, screenh)
end

function Light.EndDraw()
	glext.glUseProgram(0)
end

function Light:drawLightmap()
	glext.glUniform2f(light_shader.u_pos, self.x, self.y)
	glext.glUniform1f(light_shader.u_r, self.r)
	glext.glUniform1i(light_shader.u_texlayer, self.layer)
	
	-- Scaling factor to convert inscribed regular polygon to circumscribed
	local scale = self.r / math.cos(math.pi / Geometry.CIRCLE_SEGMENTS)
	
	glext.glUniformMatrix4fv(light_shader.u_model, 1, false, Geometry.m_model
		:identity()
		:translate(self.x, self.y, 0)
		:scale(scale,scale,1)
		:getArray())
	
	Geometry.drawCircle()
end

return Light
