
local oop = require "oop"
local ffi = require "ffi"
local bit = require "bit"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local DirLight, super = oop.Class(require "depthmap.matrixlight")

local m_light = Matrix()

function DirLight.__init(class, x, y, rot, width, length)
	local self = super.__init(class)
	self.x = x
	self.y = y
	self.rot = rot
	self.width = width
	self.length = length
	return self
end

local m1, m2 = Matrix(), Matrix()

function DirLight:getGenMatrix(matrix)
	m1:lookAt(
		self.x, self.y, 0,
		math.cos(self.rot), math.sin(self.rot), 0,
		0, 0, 1
	)
	m2:ortho(
		-self.width/2, self.width/2,
		-1, 1,
		0, self.length)
	matrix:mulInto(m2, m1)
end

function DirLight:drawLightmap(matrix)
	self:getGenMatrix(m_light)
	local m_model = Geometry.m_model:identity():translate(self.x, self.y, 0):rotate(self.rot, 0, 0, 1):scale(self.length, self.width, 1):translate(0.5,0,0)
	glext.glUniformMatrix4fv(super.LightShader.u_light, 1, false, m_light:getArray())
	glext.glUniformMatrix4fv(super.LightShader.u_model, 1, false, m_model:getArray())
	glext.glUniform1i(super.LightShader.u_texlayer, self.layer)
	
	Geometry.drawBox()
end

return DirLight
