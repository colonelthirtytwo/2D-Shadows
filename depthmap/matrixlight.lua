
local oop = require "oop"
local ffi = require "ffi"
local bit = require "bit"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()
local Shader = require "shader"
local LightBase = require "depthmap.lightbase"

local MatrixLight, super = oop.Class(LightBase)

local UBO_BINDING_POINT = 2

local depth_shader, light_shader, shadow_ubo
local lightinfo_t = ffi.typeof([[struct __attribute__ ((packed, aligned(16))) {
	float matrix[16];
	int layer;
}]])
local lightinfo_pt = ffi.typeof("$*", lightinfo_t)
local m_light = Matrix()

function MatrixLight.Init()
	assert(not depth_shader)
	
	-- -------------------------------------------------------------------------------------------------------
	local ubo_buf = ffi.new("GLuint[1]")
	glext.glGenBuffers(1, ubo_buf)
	shadow_ubo = ubo_buf[0]
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, shadow_ubo)
	
	local depth_shader_prog = Shader.new {
		[glc.GL_VERTEX_SHADER] = [[

			#version 150
			#define MAX_LIGHTS ]]..LightBase.MAX_LIGHTS..[[
			
			uniform mat4 model;
			
			struct LightInfo {
				mat4 matrix;
				int layer;
			};
			
			layout (std140) uniform LightBlock
			{
				LightInfo lights[MAX_LIGHTS];
			};
			
			in vec2 in_pos;
			in int gl_InstanceID;
			
			out vec4 gl_Position;
			out int texlayer;
			
			void main()
			{
				gl_Position = lights[gl_InstanceID].matrix * (model * vec4(in_pos, 0, 1));
				
				// Linearize depth.
				// If this is an orth matrix, w = 1, and thus no effect
				// If this is a proj matrix, cancel out the w divide to retain a linear depth buffer
				// (Projection matrix should be adjusted to account for Z being at the far plane here)
				gl_Position.z = gl_Position.z * gl_Position.w;
				
				texlayer = lights[gl_InstanceID].layer;
			}
		]],
		[glc.GL_GEOMETRY_SHADER] = [[
			#version 150
			
			layout(lines) in;
			layout(line_strip, max_vertices=2) out;
			
			in int texlayer[];
			
			void main()
			{
				gl_Layer = texlayer[0];
				gl_Position = gl_in[0].gl_Position;
				EmitVertex();
				
				gl_Layer = texlayer[1];
				gl_Position = gl_in[1].gl_Position;
				EmitVertex();
				
				EndPrimitive();
			}
		]],
		[glc.GL_FRAGMENT_SHADER] = [[
			#version 150
			out vec4 gl_FragColor;
			void main()
			{
				gl_FragColor = vec4(1,1,1,1);
			}
		]],
		
		prelink = function(prog, shaders)
			glext.glBindAttribLocation(prog, Geometry.ATTRIB_POS, "in_pos")
			glext.glBindAttribLocation(prog, Geometry.ATTRIB_TEXCOORD, "in_texcoord")
		end
	}
	glext.glUseProgram(depth_shader_prog)
	
	depth_shader = {
		prog = depth_shader_prog,
		
		u_model = glext.glGetUniformLocation(depth_shader_prog, "model"),
		
		a_pos = glext.glGetAttribLocation(depth_shader_prog, "in_pos"),
		
		b_light = glext.glGetUniformBlockIndex(depth_shader_prog, "LightBlock"),
	}
	
	-- -------------------------------------------------------------------------------------------------------
	
	glext.glUniformBlockBinding(depth_shader_prog, depth_shader.b_light, UBO_BINDING_POINT)
	
	--[[do
		local size = ffi.new("GLint[1]")
		glext.glGetActiveUniformBlockiv(depth_shader_prog, depth_shader.b_light, glc.GL_UNIFORM_BLOCK_DATA_SIZE, size)
		size = size[0]
		print(size, ffi.sizeof(lightinfo_t))
	end]]
	
	glext.glBufferData(glc.GL_UNIFORM_BUFFER, ffi.sizeof(lightinfo_t)*LightBase.MAX_LIGHTS, nil, glc.GL_STREAM_DRAW)
	glext.glBindBufferBase(glc.GL_UNIFORM_BUFFER, UBO_BINDING_POINT, shadow_ubo)
	
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, 0)
	
	-- -------------------------------------------------------------------------------------------------------
	
	local light_shader_prog = Shader.new {
		[glc.GL_VERTEX_SHADER] = [[

			#version 150
			
			uniform mat4 proj;
			uniform mat4 view;
			uniform mat4 model;
			
			uniform mat4 light;
			
			in vec2 in_pos;
			
			out vec4 gl_Position;
			out vec4 pos;
			
			void main()
			{
				vec4 modelpos = model * vec4(in_pos, 0, 1);
				pos = light*modelpos;
				pos.z = pos.z*0.5+0.5;
				gl_Position = proj * (view * modelpos);
			}
		]],
		[glc.GL_FRAGMENT_SHADER] = [[

			#version 150
			#define PI 3.1415926535897932384626433832795
			#define LIGHT_BLEED_FACTOR 25
			
			uniform sampler1DArray depthmap;
			uniform sampler2D g_diffuse;
			uniform vec4 color;
			uniform int texlayer;
			uniform vec2 screensize;
			
			smooth in vec4 pos;
			in vec4 gl_FragCoord;
			out vec4 gl_FragColor;
			
			void main()
			{
				//float amnt = step(pos.z, texture(depthmap, vec2((pos.x/pos.w)*0.5+0.5, texlayer)).x);
				float amnt = clamp( 1 - (pos.z - texture(depthmap, vec2((pos.x/pos.w)*0.5+0.5, texlayer)).x)*LIGHT_BLEED_FACTOR, 0, 1);
				amnt = amnt*(1-pos.z);
				gl_FragColor = texture(g_diffuse, vec2(gl_FragCoord)/screensize) * color * amnt;
			}
		]],
		
		prelink = function(prog, shaders)
			glext.glBindAttribLocation(prog, Geometry.ATTRIB_POS, "in_pos")
			glext.glBindAttribLocation(prog, Geometry.ATTRIB_TEXCOORD, "in_texcoord")
		end
	}
	
	glext.glUseProgram(light_shader_prog)
	glext.glUniform1i(glext.glGetUniformLocation(light_shader_prog, "depthmap"), 0)
	glext.glUniform1i(glext.glGetUniformLocation(light_shader_prog, "g_diffuse"), 1)
	glext.glUniform4f(glext.glGetUniformLocation(light_shader_prog, "color"), 1,1,1,0.2)
	
	light_shader = {
		prog = light_shader_prog,
		
		u_proj  = glext.glGetUniformLocation(light_shader_prog, "proj"),
		u_model = glext.glGetUniformLocation(light_shader_prog, "model"),
		u_view  = glext.glGetUniformLocation(light_shader_prog, "view"),
		
		u_light      = glext.glGetUniformLocation(light_shader_prog, "light"),
		u_texlayer   = glext.glGetUniformLocation(light_shader_prog, "texlayer"),
		u_screensize = glext.glGetUniformLocation(light_shader_prog, "screensize"),
		
		a_pos = glext.glGetAttribLocation(light_shader_prog, "in_pos"),
	}
	
	-- -------------------------------------------------------------------------------------------------------
	
	glext.glUseProgram(0)
	
	MatrixLight.LightShader = light_shader
end

function MatrixLight.__init(class)
	local self = super.__init(class)
	return self
end

--- Writes the shadow generation matrix, used to transform objects when rendering the shadow map
function MatrixLight:getGenMatrix(matrix)
	error("Override MatrixLight:getGenMatrix(matrix)")
end

-- -------------------------------------------------------------------------------------------------------

function MatrixLight.BeginUploadData()
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, shadow_ubo)
	local ptr = glext.glMapBufferRange(glc.GL_UNIFORM_BUFFER, 0, ffi.sizeof(lightinfo_t)*LightBase.MAX_LIGHTS, bit.bor(glc.GL_MAP_WRITE_BIT, glc.GL_MAP_INVALIDATE_BUFFER_BIT))
	assert(ptr ~= nil)
	return ffi.cast(lightinfo_pt, ptr)
end

function MatrixLight.EndUploadData()
	assert(glext.glUnmapBuffer(glc.GL_UNIFORM_BUFFER) ~= 0)
	glext.glBindBuffer(glc.GL_UNIFORM_BUFFER, 0)
end

function MatrixLight:uploadData(data, i)
	self:getGenMatrix(m_light)
	ffi.copy(data[i-1].matrix, m_light:getArray(), ffi.sizeof(Matrix))
	data[i-1].layer = self.layer
end

function MatrixLight.Preprocess(objects, num_lights)
	-- Set up
	glext.glUseProgram(depth_shader.prog)
	Geometry.instances = num_lights
	
	gl.glViewport(0,0,LightBase.SHADOW_RESOLUTION,1)
	
	for _,obj in ipairs(objects) do
		local m = obj:getModelMatrix()
		glext.glUniformMatrix4fv(depth_shader.u_model, 1, false, m:getArray())
		obj:draw(true)
	end
	
	-- Clean up
	glext.glUseProgram(0)
	Geometry.instances = nil
end

function MatrixLight.BeginDraw(m_proj, m_view, screenw, screenh)
	glext.glUseProgram(light_shader.prog)
	
	glext.glUniformMatrix4fv(light_shader.u_proj, 1, false, m_proj:getArray())
	glext.glUniformMatrix4fv(light_shader.u_view, 1, false, m_view:getArray())
	glext.glUniform2f(light_shader.u_screensize, screenw, screenh)
end

function MatrixLight.EndDraw()
	glext.glUseProgram(0)
end

function MatrixLight:drawLightmap()
	error("Override MatrixLight:drawLightmap()")
end

--[[function MatrixLight:drawLightmap()
	self:getMatrix(m_light)
	glext.glUniformMatrix4fv(light_shader.u_light, 1, false, m_light:getArray())
	glext.glUniform1i(light_shader.u_texlayer, self.layer)
	
	glext.glUniformMatrix4fv(light_shader.u_model, 1, false, Geometry.m_model
		:identity()
		:translate(self.x, self.y, 0)
		:scale(self.r*2,self.r*2,1)
		:getArray())
	
	Geometry.drawBox()
end]]

return MatrixLight
