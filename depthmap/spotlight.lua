
local oop = require "oop"
local ffi = require "ffi"
local bit = require "bit"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local SpotLight, super = oop.Class(require "depthmap.matrixlight")

local m_light = Matrix()

function SpotLight.__init(class, x, y, rot, fieldofview, length)
	local self = super.__init(class)
	self.x = x
	self.y = y
	self.rot = rot
	self.fov = fieldofview
	self.length = length
	return self
end

local function linear_perspective(self, fovy, aspect, near, far)
	-- Sets up a matrix that helps preserve a linear depth buffer (instead of inverse).
	-- A vertex shader needs to multiply Z by W after the transformation for this to work.
	local tanfov = math.tan(fovy / 2)
	
	self:set(0,0, 1/(aspect * tanfov))
	self:set(0,1, 0)
	self:set(0,2, 0)
	self:set(0,3, 0)
	
	self:set(1,0, 0)
	self:set(1,1, 1/(tanfov))
	self:set(1,2, 0)
	self:set(1,3, 0)
	
	self:set(2,0, 0)
	self:set(2,1, 0)
	self:set(2,2, -2/(far-near))
	self:set(2,3, -1)
	
	self:set(3,0, 0)
	self:set(3,1, 0)
	self:set(3,2, -(far+near)/(far-near))
	self:set(3,3, 0)
end

local m1, m2 = Matrix(), Matrix()

function SpotLight:getGenMatrix(matrix)
	m1:lookAt(
		self.x, self.y, 0,
		math.cos(self.rot), math.sin(self.rot), 0,
		0, 0, 1
	)
	linear_perspective(m2, self.fov, 1, 1, self.length)
	
	matrix:mulInto(m2, m1)
end

function SpotLight:drawLightmap(matrix)
	self:getGenMatrix(m_light)
	local yscale = self.length*2*math.tan(self.fov/2)
	local m_model = Geometry.m_model:identity():translate(self.x, self.y, 0):rotate(self.rot, 0, 0, 1):scale(self.length, yscale, 1)
	glext.glUniformMatrix4fv(super.LightShader.u_light, 1, false, m_light:getArray())
	glext.glUniformMatrix4fv(super.LightShader.u_model, 1, false, m_model:getArray())
	glext.glUniform1i(super.LightShader.u_texlayer, self.layer)
	
	Geometry.drawTri()
end

return SpotLight

