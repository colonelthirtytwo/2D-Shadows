
local oop = require "oop"
local ffi = require "ffi"
local bit = require "bit"
local Matrix = require "matrix"
local Geometry = require "geometry"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()
local Shader = require "shader"

local LightBase, super = oop.Class()

local SHADOW_RESOLUTION = 1024
local MAX_LIGHTS = 64

LightBase.SHADOW_RESOLUTION = SHADOW_RESOLUTION
LightBase.MAX_LIGHTS = MAX_LIGHTS

function LightBase.Init(width, height)
	assert(not LightBase.ShadowTex, "Double-init of LightBase")
	
	do
		local max_attachments_buf = ffi.new("GLint[1]")
		gl.glGetIntegerv(glc.GL_MAX_COLOR_ATTACHMENTS, max_attachments_buf)
		assert(max_attachments_buf[0] >= 2, "Please upgrade your drivers or graphics card (GL_MAX_COLOR_ATTACHMENTS < 2)")
	end
	
	local tex_buf = ffi.new("GLuint[3]")
	gl.glGenTextures(3, tex_buf)
	
	local fbo_buf = ffi.new("GLuint[2]")
	glext.glGenFramebuffers(2, fbo_buf)
	
	local rbo_buf = ffi.new("GLuint[1]")
	glext.glGenRenderbuffers(1, rbo_buf)
	
	-- -------------------------------------------------------------------------------------------------------
	-- Shadow Map
	
	LightBase.ShadowTex = tex_buf[0]
	gl.glBindTexture(glc.GL_TEXTURE_1D_ARRAY, LightBase.ShadowTex)
	gl.glTexParameterf(glc.GL_TEXTURE_1D_ARRAY, glc.GL_TEXTURE_WRAP_S, glc.GL_REPEAT)
	gl.glTexParameteri(glc.GL_TEXTURE_1D_ARRAY, glc.GL_TEXTURE_MIN_FILTER, glc.GL_LINEAR)
	gl.glTexParameteri(glc.GL_TEXTURE_1D_ARRAY, glc.GL_TEXTURE_MAG_FILTER, glc.GL_LINEAR)
	gl.glTexImage2D(glc.GL_TEXTURE_1D_ARRAY, 0, glc.GL_DEPTH_COMPONENT, SHADOW_RESOLUTION, MAX_LIGHTS,
		0, glc.GL_DEPTH_COMPONENT, glc.GL_UNSIGNED_BYTE, nil)
	
	LightBase.AvailableIDs = {}
	for i=MAX_LIGHTS-1,0,-1 do
		table.insert(LightBase.AvailableIDs, i)
	end
	
	LightBase.ShadowFBO = fbo_buf[0]
	
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, LightBase.ShadowFBO)
	glext.glFramebufferTexture(glc.GL_FRAMEBUFFER, glc.GL_DEPTH_ATTACHMENT, LightBase.ShadowTex, 0)
	assert(glext.glCheckFramebufferStatus(glc.GL_FRAMEBUFFER) == glc.GL_FRAMEBUFFER_COMPLETE)
	
	gl.glBindTexture(glc.GL_TEXTURE_1D_ARRAY, 0)
	
	-- -------------------------------------------------------------------------------------------------------
	-- GBuffer FBO
	
	LightBase.GBufferFBO = fbo_buf[1]
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, LightBase.GBufferFBO)
	
	LightBase.GBufferColorTex = tex_buf[1]
	gl.glBindTexture(glc.GL_TEXTURE_2D, LightBase.GBufferColorTex)
	gl.glTexParameterf(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_WRAP_S, glc.GL_CLAMP_TO_EDGE)
	gl.glTexParameterf(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_WRAP_T, glc.GL_CLAMP_TO_EDGE)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MIN_FILTER, glc.GL_NEAREST)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MAG_FILTER, glc.GL_NEAREST)
	gl.glTexImage2D(glc.GL_TEXTURE_2D, 0, glc.GL_RGBA, width, height,
		0, glc.GL_RGBA, glc.GL_UNSIGNED_BYTE, nil)
	glext.glFramebufferTexture(glc.GL_FRAMEBUFFER, glc.GL_COLOR_ATTACHMENT0, LightBase.GBufferColorTex, 0)
	
	--[[
	LightBase.GBufferNormalsTex = tex_buf[2]
	gl.glBindTexture(glc.GL_TEXTURE_2D, LightBase.GBufferNormalsTex)
	gl.glTexParameterf(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_WRAP_S, glc.GL_CLAMP_TO_EDGE)
	gl.glTexParameterf(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_WRAP_T, glc.GL_CLAMP_TO_EDGE)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MIN_FILTER, glc.GL_NEAREST)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MAG_FILTER, glc.GL_NEAREST)
	gl.glTexImage2D(glc.GL_TEXTURE_2D, 0, glc.GL_RGB, width, height,
		0, glc.GL_RGB, glc.GL_UNSIGNED_BYTE, nil)
	glext.glFramebufferTexture(glc.GL_FRAMEBUFFER, glc.GL_COLOR_ATTACHMENT1, LightBase.GBufferNormalsTex, 0)
	]]
	
	LightBase.GBufferDepthRBO = rbo_buf[0]
	glext.glBindRenderbuffer(glc.GL_RENDERBUFFER, LightBase.GBufferDepthRBO)
	glext.glRenderbufferStorage(glc.GL_RENDERBUFFER, glc.GL_DEPTH_COMPONENT, width, height)
	glext.glFramebufferRenderbuffer(glc.GL_FRAMEBUFFER, glc.GL_DEPTH_ATTACHMENT, glc.GL_RENDERBUFFER, LightBase.GBufferDepthRBO)
	glext.glBindRenderbuffer(glc.GL_RENDERBUFFER, 0)
	
	--glext.glDrawBuffers(2, ffi.new("GLuint[2]", glc.GL_COLOR_ATTACHMENT0, glc.GL_COLOR_ATTACHMENT1))
	
	assert(glext.glCheckFramebufferStatus(glc.GL_FRAMEBUFFER) == glc.GL_FRAMEBUFFER_COMPLETE)
	
	gl.glBindTexture(glc.GL_TEXTURE_2D, 0)
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, 0)
end

function LightBase.BeginRender()
	gl.glEnable(glc.GL_DEPTH_TEST)
	gl.glColorMask(false, false, false, false)
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, LightBase.ShadowFBO)
	gl.glClear(glc.GL_DEPTH_BUFFER_BIT)
end

function LightBase.EndRender(screen_w, screen_h)
	gl.glColorMask(true, true, true, true)
	gl.glDisable(glc.GL_DEPTH_TEST)
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, 0)
	gl.glViewport(0,0,screen_w,screen_h)
end

function LightBase.BeginGBuffer(m_proj, m_view)
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, LightBase.GBufferFBO)
	gl.glClear(bit.bor(glc.GL_COLOR_BUFFER_BIT, glc.GL_DEPTH_BUFFER_BIT))
	Geometry.beginDrawing(m_proj, m_view)
end

function LightBase.EndGBuffer()
	Geometry.endDrawing()
	glext.glBindFramebuffer(glc.GL_FRAMEBUFFER, 0)
end

-- -------------------------------------------------------------------------------------------------------

function LightBase.__init(class, ...)
	local self = super.__init(class, ...)
	self.layer = assert(table.remove(LightBase.AvailableIDs), "Max lights exceeded")
	return self
end

function LightBase:destroy()
	table.insert(LightBase.AvailableIDs, self.layer)
	self.layer = nil
end

return LightBase
