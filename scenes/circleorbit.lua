
local oop = require "oop"

local CircleOrbit, super = oop.Class(require "scene")

local Circle = require "objects.circle"

function CircleOrbit.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	
	self.objects[1] = Circle(0,0,30)
	self.lights[1] = renderer.createLight("point", -150, 0, 1000)
	
	return self
end

function CircleOrbit:think(window, t)
	self.lights[1].x = math.cos(t)*100
	self.lights[1].y = math.sin(t)*100
end

return CircleOrbit
