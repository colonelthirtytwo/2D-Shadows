
local oop = require "oop"

local DirLightTest, super = oop.Class(require "scene")

local Box = require "objects.box"
local Circle = require "objects.circle"

function DirLightTest.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	for i=1,4 do
		local theta = 2*math.pi*i/4
		self.objects[i] = Box(math.cos(theta)*100,math.sin(theta)*100,30,30)
	end
	
	for i=1,4 do
		local theta = 2*math.pi*i/4 + math.pi/4
		self.objects[i+4] = Circle(math.cos(theta)*200,math.sin(theta)*200,15)
	end
	
	self.lights[1] = renderer.createLight("directional", 0, 0, 0, 100, 300)
	
	return self
end

function DirLightTest:think(window, t)
	self.lights[1].rot = t
end

return DirLightTest
