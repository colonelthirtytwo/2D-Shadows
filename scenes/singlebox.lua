
local oop = require "oop"

local SingleBox, super = oop.Class(require "scene")

local Box = require "objects.box"

function SingleBox.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	self.objects[1] = Box(0,0,30,30)
	self.lights[1] = renderer.createLight("point", -150, 0, 1000)
	
	return self
end

return SingleBox
