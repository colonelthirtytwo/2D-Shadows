
local oop = require "oop"

local Stress, super = oop.Class(require "scene")

local Box = require "objects.box"

local NUM_LIGHTS = 64
local NUM_SPIRAL = 2056*4

local function pos(i, t)
	local n = 0.5*i/(2*math.pi) + t
	return math.cos(n)*(i*0.3+150), math.sin(n)*(i*0.3+150)
end

function Stress.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	for i=1,NUM_LIGHTS do
		self.lights[i] = renderer.createLight("point", 0,0,500)
	end
	
	for i=1,NUM_SPIRAL do
		local x, y = pos(i, 0)
		self.objects[i] = Box(x ,y, 10, 10)
	end
	
	return self
end

function Stress:think(window, t)
	for i,obj in ipairs(self.objects) do
		local x, y = pos(i, t)
		obj.x = x
		obj.y = y
	end
	
	for i,light in ipairs(self.lights) do
		local theta = 2*math.pi*i/#self.lights - t
		light.x = 100*math.cos(theta)
		light.y = 100*math.sin(theta)
	end
end

return Stress
