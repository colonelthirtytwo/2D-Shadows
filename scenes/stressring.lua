
local oop = require "oop"

local StressRing, super = oop.Class(require "scene")

local Circle = require "objects.circle"

local NUM = 64

function StressRing.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	for i=1,NUM do
		self.objects[i] = Circle(0,0,10)
		self.lights[i] = renderer.createLight("point", 0,0,500)
	end
	
	return self
end

function StressRing:think(window, t)
	for i,obj in ipairs(self.objects) do
		local theta = t + 2*math.pi*i/#self.objects
		obj.x = 300*math.cos(theta)
		obj.y = 300*math.sin(theta)
	end
	
	for i,light in ipairs(self.lights) do
		local theta = 2*math.pi*i/#self.lights - t
		light.x = 100*math.cos(theta)
		light.y = 100*math.sin(theta)
	end
end

return StressRing
