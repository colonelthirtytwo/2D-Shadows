
local oop = require "oop"

local HugeBox, super = oop.Class(require "scene")

local Box = require "objects.box"

function HugeBox.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	self.objects[1] = Box(0, -300, 1000, 300)
	self.objects[2] = Box(0,  300, 1000, 300)
	self.lights[1] = renderer.createLight("point", 0, 0, 1000)
	
	return self
end

function HugeBox:think(window, t)
end

return HugeBox
