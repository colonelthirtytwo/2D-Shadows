
local oop = require "oop"

local BoxRing, super = oop.Class(require "scene")

local Box = require "objects.box"

function BoxRing.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	for i=1,4 do
		self.objects[i] = Box(0,0,30,30)
	end
	self.lights[1] = renderer.createLight("point", -150, 0, 1000)
	
	return self
end

function BoxRing:think(window, t)
	for i,obj in ipairs(self.objects) do
		local theta = t + 2*math.pi*i/#self.objects
		obj.x = math.cos(theta)*100
		obj.y = math.sin(theta)*100
	end
end

return BoxRing
