
local oop = require "oop"

local Rings, super = oop.Class(require "scene")

local Circle = require "objects.circle"

local NUM = 20

function Rings.__init(class, renderer)
	local self = super.__init(class, renderer)
	
	self.lights[1] = renderer.createLight("point", 0,0,500)
	
	for i=1,5+10+15+20+25 do
		self.objects[i] = Circle(0,0,10)
	end
	
	return self
end

function Rings:think(window, t)
	for i=1,5 do
		local obj = self.objects[i]
		local theta = t + 2*math.pi*i/5
		obj.x = 50*math.cos(theta)
		obj.y = 50*math.sin(theta)
	end
	
	for i=1,10 do
		local obj = self.objects[i+5]
		local theta = -t + 2*math.pi*i/10
		obj.x = 100*math.cos(theta)
		obj.y = 100*math.sin(theta)
	end
	
	for i=1,15 do
		local obj = self.objects[i+5+10]
		local theta = t*1.5 + 2*math.pi*i/15
		obj.x = 150*math.cos(theta)
		obj.y = 150*math.sin(theta)
	end
	
	for i=1,20 do
		local obj = self.objects[i+5+10+15]
		local theta = -t*1.5 + 2*math.pi*i/20
		obj.x = 200*math.cos(theta)
		obj.y = 200*math.sin(theta)
	end
	
	for i=1,25 do
		local obj = self.objects[i+5+10+15+20]
		local theta = t*2 + 2*math.pi*i/25
		obj.x = 250*math.cos(theta)
		obj.y = 250*math.sin(theta)
	end
end

return Rings
