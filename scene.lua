
local oop = require "oop"

local Scene, super = oop.Class()

function Scene.__init(class, renderer)
	local self = super.__init(class)
	
	self.objects = {}
	self.lights = {}
	
	return self
end

function Scene:think(window, t)
end

function Scene:destroy()
	for _,light in ipairs(self.lights) do
		light:destroy()
	end
	
	for _,object in ipairs(self.objects) do
		object:destroy()
	end
	
	self.objects = nil
	self.lights = nil
end

return Scene
