
do return end

local ffi = require "ffi"
local lj_glfw = require "glfw"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

jit.off()

gl.glEnable(glc.GL_DEBUG_OUTPUT_SYNCHRONOUS)

local function glcallback(source, typ, id, severity, length, message, _)
	local msg = ffi.string(message, length)
	if severity == glc.GL_DEBUG_SEVERITY_HIGH then
		error("OpenGL Error: "..msg, 3)
	else
		print("OpenGL Message: "..msg)
	end
end

glext.glDebugMessageCallback(glcallback, nil)
